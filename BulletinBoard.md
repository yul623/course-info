# CSE 262 - Bulletin Board

## :computer: Homework

- [ ] 09/27 - [Homework 5](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-5) - Rust II - A Lexer
- [x] 09/20 - [Homework 4](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-4) - Rust I - Rust Fundamentals
- [x] 09/13 - [Homework 3](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-3) - Python II - Fun with Python and Spotify
- [x] 09/06 - [Homework 2](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-2) - Python I - Java to Ptyhon Translation
- [x] 08/30 - [Homework 1](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-1) - Assignment submission process
- [x] 08/30 - [Homework 0](https://gitlab.com/lehigh-cse-262/fall-2021/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab
- [ ] 12/03 - [Participation](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/participation)

## :checkered_flag: Quizzes

- [ ] 09/24 - [Quiz 4](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-4)
- [x] 09/17 - [Quiz 3](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-3)
- [x] 09/10 - [Quiz 2](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-2)
- [x] 09/03 - [Quiz 1](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-1)

## :books: Readings

| Readings           |
| ------------------ |
|**Week 4**|
| <ul><li>[The Rust Book](https://doc.rust-lang.org/stable/book/ch03-00-common-programming-concepts.html) - Chapters 3 - 10. The way I would approach this reading is to do it side-by-side with the homework. Go to the associated section in the book and use it to help you solve the problems. It's not important if you don't read every word in all these chapters. Read enough to help you pass the HW tests.</li></ul>|
|**Week 3**|
| <ul><li>[A Byte of Python](https://python.swaroopch.com/) - Modules through Standard Library</li><li>[Python Data Science Tutorial](https://www.tutorialspoint.com/python_data_science/index.htm)</li><li>[Matplotlib Tutorials](https://matplotlib.org/stable/tutorials/index.html)</li></ul>|
|**Week 2**|
|<ul><li>[A Byte of Python](https://python.swaroopch.com/) - Introduction through Functions</li><li>[Differences Between Python 2 and 3](https://www.thecrazyprogrammer.com/2018/01/difference-python-2-3.html)</li><li>[The Zen of Python](https://www.python.org/dev/peps/pep-0020/)</li><ul>
|**Week 1**|
|[The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6

## :vhs: Lectures
- [FA21 Recordings](https://drive.google.com/drive/folders/1-HEz-t5_-ITRr54d166AAsK5qj_cRdLW?usp=sharing)
- [FA20 Playlist](https://www.youtube.com/playlist?list=PL4A2v89SXU3Trn3mBoocRdPwDULMyk_id)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
|Lecture 10 | 09/22 | Rust IV| [Video](https://drive.google.com/file/d/1bqJCkM5tkoRJ-B-fCz3PSaMlLgekfwpE/view?usp=sharing)
|Lecture 09 | 09/20 | Rust III | [Video](https://drive.google.com/file/d/18AWY5PfhqWYpWu2m9QRuMGcu2KxkUedt/view?usp=sharing)
|Lecture 08 | 09/15 | Rust II | [Video](https://drive.google.com/file/d/1QWHzR7YwWlKX394qUU_9_zYf1kjQv9na/view?usp=sharing)
|Lecture 07 | 09/13 | Rust I | [Video](https://drive.google.com/file/d/10g56Lh1TaaMttgeKYpwx50NOfo7PFfoS/view?usp=sharing)
|Lecture 06 | 09/08 | Python IV | [Video](https://drive.google.com/file/d/167XqFJb4vZOvy5Fo77fOHujeH-s_HM8i/view?usp=sharing)
|Lecture 05 | 09/06 | Python III | [Video](https://drive.google.com/file/d/1JJUGk2eOxuKVwS0-u0XysMeFEvrdcbk0/view?usp=sharing)
|Lecture 04 | 09/01 | Python II | [Video](https://drive.google.com/file/d/1ZfAbDGFMeqn5vKDykc-TfWtKBCufAXHi/view?usp=sharing)
|Lecture 03 | 08/30 | Python I | [Video](https://drive.google.com/file/d/1Z7P5oyqA9aqQth0e5yvixQLOJCC0GywV/view?usp=sharing)
|Lecture 02 | 08/25 | Git and Gitlab Fundamentals | [Video](https://drive.google.com/file/d/1-9dQwGxY6HrPkvGJUr9JbtTJb98wj1xk/view?usp=sharing)
|Lecture 01 | 08/23 | Course Introduction | [Video](https://drive.google.com/file/d/1IOioqiKDanjnlOhy4YM3O6MtxdxPerQv/view?usp=sharing)
