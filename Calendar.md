# Course Calendar

Below is a tentative schedule for the semester, broken down by week. Topics covered and the order in which they are covered is subjet to change. Any changes will be reflected here. It is your responsibility to stay up to date with this calendar over the course of this semester.


| Week                      | Monday        | Wednesday          | Friday          |
| ------------------------- | --------------| ------------------ | --------------- |
| Week 1 (Aug 23 - Aug 27)  | Syllabus day  | Intro to Git and Gitlab | Recitation 1    |
| Week 2 (Aug 30 - Sep 03)  | Python I - Intro to Python      | Python II - Python vs. Java         | Recitation 2    |
| Week 3 (Sep 06 - Sep 10)  | Python III - Design and Implementation    | Python IV - Ecosystem          | Recitation 3    |
| Week 4 (Sep 13 - Sep 17)  | Rust I        | Rust II            | Recitation 4    |
| Week 5 (Sep 20 - Sep 24)  | Rust III      | Rust IV            | Recitation 5    |
| Week 6 (Sep 27 - Oct 01)  | Rust V        | Rust VI            | Recitation 6    |
| Week 7 (Oct 04 - Oct 08)  | Rust VII      | Rust VIII          | Recitation 7    |
| Week 8 (Oct 11 - Oct 15)  | Pacing Break (No Class) | Rust IX  | Recitation 8    |
| Week 9 (Oct 18 - Oct 22)  | Haskell I     | Haskell II         | Recitation 9    |
| Week 10 (Oct 25 - Oct 29) | Haskell III     | Haskell IV       | Recitation 10    |
| Week 11 (Nov 01 - Nov 05) | Haskell V     | Haskell VI         | Recitation 11*    |
| Week 12 (Nov 08 - Nov 12) | Prolog I      | Prolog II          | Recitation 12    |
| Week 13 (Nov 15 - Nov 19) | Prolog III    | Prolog IV          | Recitation 13    |
| Week 14 (Nov 22 - Nov 26) | Future of Programming | Thanksgiving Break (No Class) | Thanksgiving Break (No Class) |
| Week 15 (Nov 29 - Dec 03) | Future of Programming | Future of Programming      | Final Review    |

For your reference: [Lehigh University Academic Calendar](https://ras.lehigh.edu/content/current-students/academic-calendar)

Important dates:

- First day of class: August 23
- *Last day to withdraw with a "W": November 5
- Last day of class: December 3
