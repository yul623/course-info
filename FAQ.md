# CSE 262 Programming Languages - Frequently Asked Questions (FAQ)

## Q1. How many comments should I write?

I won't prescribe a number of comments to write, but I will say that if want to write good code and you are writing 0 comments, then writing more comments is the best thing you can do to increase your code quality. Also, you probably can't write too many comments in your code, as far as this class and I am concerned. I like to see your thought process. If you can't get a problem 100% right, it's helpful to write down pseudo code or a partial solution as a comment. This can help you earn partial credit where appropriate.

In general, good program source contains both:
- Code – explains the “what” and “how” of the program
    what does it do? 
    how does it work?
- Comments – explain the “why” of the program
    Explain any esoteric bits of code
    To point out something to the reader
    Or to remember something (TODO)
